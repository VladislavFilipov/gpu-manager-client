import React from 'react';

import SideBar from './SideBar/SideBar';
import Workspace from './Workspace/Workspace';

import './App.scss';

const url = process.env.REACT_APP_URL;
const port = process.env.REACT_APP_PORT;

const pages = [
    <Workspace num={1} url={url} port={port} />,
    // <Processes num={2} url={url} port={port} />
]

class App extends React.Component {

    state = {
        activePage: 1
    }

    chosePage = (num) => {
        this.setState({
            activePage: num
        });
    }

    render() {
        return (
            <div className="app">
                <SideBar chosePage={this.chosePage} activePage={this.state.activePage} />
                <div className="app__main">
                    <div className="app__header">Какое-то название необычным шрифтом</div>
                    {/* <Workspace url={url} port={port} /> */}
                    {pages[this.state.activePage - 1]}
                </div>

            </div>
        );
    }
}

export default App;