import React from 'react';
import axios from 'axios';

import './Form.scss';

class Form extends React.Component {

    state = {
        // createTask: false
    }

    onCancelButtonClick = () => {
        this.clear();
        this.props.exitForm('cancel');
    }

    onSaveButtonClick = () => {

        // console.log(document.querySelector('.form__user-select').value);

        if (this.props.chosenCards === [] ||
            document.querySelector('.form__desc input').value === '' ||
            document.querySelector('.form__time-inputs').firstChild.value === '' ||
            document.querySelector('.form__time-inputs').lastChild.value === '' ||
            document.querySelector('.form__user-select').value === 0) {

            document.querySelector('.form__warning').style.display = 'inline';
            return;
        } else {
            document.querySelector('.form__warning').style.display = 'none';
        }

        let
            date = document.querySelector('.form__time-inputs').firstChild.value,
            time = document.querySelector('.form__time-inputs').lastChild.value;

        console.log(document.querySelector('.form__user-select').value);

        axios.post(`http://${this.props.url}:${this.props.port}/` +
            `${this.props.editableProcess ? 'update-task' : 'add-task'}`, {
                id: this.props.editableProcess,
                desc: document.querySelector('.form__desc input').value,
                time_end: Date.parse(date + 'T' + time) / 1000,
                cards: '' + this.props.chosenCards.sort((a, b) => a - b),
                cardsId: this.props.chosenCards.sort((a, b) => a - b),
                user: document.querySelector('.form__user-select').value
            })
            .then(() => {
                this.clear();
                // this.getCards();
                this.props.exitForm('save');
            })
            .catch(console.log);

    }

    clear = () => {
        document.querySelector('.form__desc input').value = '';
        document.querySelector('.form__time-inputs').firstChild.value = '';
        document.querySelector('.form__time-inputs').lastChild.value = '';
    }

    dateParse = (sec) => {
        let date = new Date(sec * 1000);
        let month = date.getMonth() === 11 ? 0 : date.getMonth() + 1;
        return {
            date:
                `${date.getFullYear()}-` +
                `${month < 10 ? '0' + month : month}-` +
                `${date.getDate() < 10 ? '0' + date.getDate() : date.getDate()}`,
            time:
                `${date.getHours() < 10 ? '0' + date.getHours() : date.getHours()}:` +
                `${date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()}`
        };
    }

    componentDidMount() {
        if (this.props.editableProcess) {

            axios.post(`http://${this.props.url}:${this.props.port}/process-by-id`, { id: this.props.editableProcess })
                .then((res) => {

                    let process = res.data[0];

                    console.log(process.cards);

                    document.querySelector('.form__desc input')
                        .value = process.desc;
                    document.querySelector('.form__user-select')
                        .value = process.user_id;

                    document.querySelector('.form__time-inputs').firstChild
                        .value = this.dateParse(process.time_end).date;
                    document.querySelector('.form__time-inputs').lastChild
                        .value = this.dateParse(process.time_end).time;

                    axios.post(`http://${this.props.url}:${this.props.port}/free-cards`, {
                        cardsId: process.cards.split(',')
                    })
                        .then(() => {
                            this.props.exitForm('edit');
                        })
                        .catch(console.log);

                    this.props.returnChosenCards(this.props.cards
                        .filter(card => process.cards.includes(card.id))
                        .map(card => card.id)
                    );
                })
                .catch(console.log);


        }
    }

    render() {
        return (
            <div className="form">

                <div className="form__cards-field">
                    Необходимые карты (выберете щелчком мыши в таблице)
                            <div className="form__chosen-cards">
                        {this.props.cards
                            .filter(card => this.props.chosenCards.includes(card.id))
                            .map((card, i) => (
                                <div key={i} className="form__chosen-card">
                                    <span>
                                        {this.props.servers
                                            .find(server => server.id === card.server_id)
                                            .name}
                                    </span>
                                    <span>{card.name}</span>
                                    <span
                                        onClick={() => {
                                            let chCards = [...this.props.chosenCards];
                                            chCards.splice(chCards.indexOf(card.id), 1);
                                            this.props.returnChosenCards(chCards);
                                        }}
                                    >+</span>
                                </div>
                            ))}
                    </div>
                </div>
                <div className="form__desc">
                    Краткое описание задачи
                            <input type="text" placeholder="Введите описание" />
                </div>
                <div className="form__time">
                    <span>Оценочная дата завершения</span>
                    <div className="form__time-inputs">
                        <input type="date" />
                        <input type="time" />
                    </div>
                </div>
                <div className="form__user">
                    <span>Пользователь</span>
                    <select className="form__user-select">
                        <option value={0}>- Не выбрано -</option>
                        {
                            this.props.users.map((user, i) => (
                                <option key={i} value={user.id}>{user.login}</option>
                            ))
                        }
                    </select>
                </div>
                <div className="form__buttons">
                    <span>
                        <span className="form__warning">Заполните все поля!</span>
                        <button
                            className="form__button form__button_save"
                            onClick={this.onSaveButtonClick}
                        >Сохранить</button>
                        <button
                            className="form__button form__button_cancel"
                            onClick={this.onCancelButtonClick}
                        >Отменить</button>

                    </span>
                </div>
            </div>
        );
    }
}

export default Form;