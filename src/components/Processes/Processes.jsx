import React from 'react';
import axios from 'axios';

import './Processes.scss';

class Processes extends React.Component {

    state = {
        processes: []
    }

    getProcesses = () => {
        axios.get(`http://${this.props.url}:${this.props.port}/processes`)
            .then((res) => {
                let [processes, cardsInfo] = res.data;
                // console.log(processes);

                processes.forEach(process => {
                    process.cardsId = process.cards;
                    process.cards = process.cards.split(',').map(card => {
                        let res;
                        cardsInfo.forEach(info => {
                            if (info.id === +card) {
                                res = info;
                            }
                        });
                        return res;
                    });

                    process.time_end = this.getDateView(process.time_end);
                    // console.log(process.time_end);
                });

                // console.log(processes);

                this.setState({
                    processes: processes
                })
            })
            .catch(console.log);
    }

    componentDidMount() {
        this.getProcesses();
    }

    componentDidUpdate(prevProps) {
        if (prevProps !== this.props && !this.props.createTask) {
            this.getProcesses();
        }
    }

    getDateView = (sec) => {
        let date = new Date(sec * 1000);
        let month = date.getMonth() === 11 ? 0 : date.getMonth() + 1;
        return `${date.getDate() < 10 ? '0' + date.getDate() : date.getDate()}.` +
            `${month < 10 ? '0' + month : month}.` +
            `${date.getFullYear()} - ` +
            `${date.getHours() < 10 ? '0' + date.getHours() : date.getHours()}:` +
            `${date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes()}`;
    }

    deleteProcess = (e) => {
        let temp = this.state.processes.find(process => process.id === +e.currentTarget.dataset.id);
        axios.post(`http://${this.props.url}:${this.props.port}/delete-task`, {
            processId: e.currentTarget.dataset.id,
            cardsId: temp.cardsId
        })
            .then(() => {
                this.getProcesses();
                this.props.processDidDeleted();
            })
            .catch(console.log);
    }

    editProcess = (e) => {
        this.props.processWillEdit(
            this.state.processes
                .find(process => process.id === +e.currentTarget.dataset.id)
                .id
        );
    }

    render() {
        return (
            <div className="processes">
                <div className="processes__title">Активные процессы</div>
                <ul className="processes__list">
                    <li className="processes__list-item processes__list-item_head">
                        <span>Описание</span>
                        <span>Используемые видеокарты</span>
                        <span>Дата завершения</span>
                        <span>Пользователь</span>

                    </li>
                    {
                        this.state.processes.map((process, i) => (
                            <li key={i} className="processes__list-item">
                                <span>{process.desc}</span>
                                <span>
                                    {
                                        process.cards.map((card, j) => (
                                            <div key={j}>{card.host} - {card.name}</div>
                                        ))
                                    }
                                </span>
                                <span>{process.time_end}</span>
                                <span>{process.login}</span>
                                <span
                                    data-id={process.id}
                                    className="processes__edit"
                                    onClick={this.editProcess}
                                >
                                    <i className="fas fa-edit"></i>
                                </span>
                                <span
                                    data-id={process.id}
                                    className="processes__delete"
                                    onClick={this.deleteProcess}
                                >
                                    <i className="fas fa-times-circle"></i>
                                </span>
                            </li>
                        ))
                    }

                </ul>
            </div>
        );
    }
}

export default Processes;