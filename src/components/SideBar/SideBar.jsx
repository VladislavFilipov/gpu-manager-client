import React from 'react';

import './SideBar.scss';

class SideBar extends React.Component {
    render() {
        return (
            <div className="sidebar">
                <div className="sidebar__head">Лого</div>
                <div className="sidebar__body">
                    <ul className="sidebar__list">
                        <li
                            className={`sidebar__list-item ${this.props.activePage === 1 ? 'active' : ''}`}
                            onClick={() => this.props.chosePage(1)}
                        >
                            Главная
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default SideBar;