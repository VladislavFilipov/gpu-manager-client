import React from 'react';

import './Table.scss';

class Table extends React.Component {

    state = {
        // servers: [],
        chosen: []
    }

    componentDidMount() {

    }

    componentDidUpdate(prevProps) {
        if (this.props !== prevProps) {
            this.setState({
                chosen: prevProps.chosenCards
            })
        }
    }

    onCardMouseOver = (e) => {
        e.currentTarget.querySelector('.card__tooltip').style.display = 'block';
    }

    onCardMouseOut = (e) => {
        e.currentTarget.querySelector('.card__tooltip').style.display = 'none';
    }

    onCardClick = (e) => {
        if (e.currentTarget.classList.contains('table__card_free')) {
            e.currentTarget.classList.toggle('active');

            if (!this.props.chosenCards.includes(+e.currentTarget.dataset.id)) {
                this.props.returnChosen(this.props.chosenCards.concat(+e.currentTarget.dataset.id))
            } else {
                let chCards = [...this.props.chosenCards];
                chCards.splice(chCards.indexOf(+e.currentTarget.dataset.id), 1);
                this.props.returnChosen(chCards);
            }

        }
    }


    render() {
        let cards = this.props.cards;
        let servers = this.props.servers;

        return (
            <div className="table">

                {
                    servers.map((server, i) => (
                        <div key={i} className="table__row">
                            <div className="table__server-name">{server.name}</div>
                            {
                                cards.filter(card => card.server_id === server.id)
                                    .map((card, j) => (<div
                                        key={j}
                                        data-id={card.id}
                                        className={`table__card ` +
                                            `table__card_${card.status} ` +
                                            `${this.props.chosenCards.includes(+card.id) ? 'active' : ''}`
                                        }
                                        onMouseOver={this.onCardMouseOver}
                                        onMouseOut={this.onCardMouseOut}
                                        onClick={this.props.mode === 'view' ? () => { } : this.onCardClick}
                                    >

                                        <span>{card.name}</span>
                                        {/* <span>Fan: {card.params.Fan}</span> */}

                                        {/* <div
                                            className="card__bg"
                                            style={{ width: `${parseFloat(card.params.Fan)}%` }}
                                        ></div> */}
                                        <div className="card__tooltip">
                                            <div>Fan: {card.params.Fan}</div>
                                            <div>Temp: {card.params.Temp}</div>
                                            <div>Perf: {card.params.Perf}</div>
                                            <div>Pwr: {card.params.Pwr}</div>
                                            <div>Memory Usage: {card.params.MemUsage}</div>
                                            <div>GPU Util: {card.params.GpuUtil}</div>
                                        </div>
                                    </div>)
                                    )
                            }
                        </div>)
                    )
                }
            </div>
        );
    }
}

export default Table;