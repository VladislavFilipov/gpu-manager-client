import React from 'react';
import axios from 'axios';

import Form from '../Form/Form';
import Table from '../Table/Table';
import Processes from '../Processes/Processes';

import './Workspace.scss';

class Workspace extends React.Component {

    state = {
        createTask: false,
        editableProcess: null,
        servers: [],
        cards: [],
        users: [],
        chosenCards: []
    }

    getServers = () => {
        axios.get(`http://${this.props.url}:${this.props.port}/servers`)
            .then((res) => {
                // console.log(res.data);
                this.setState({
                    servers: res.data
                })
            })
            .catch(console.log);
    }

    getCards = () => {
        axios.get(`http://${this.props.url}:${this.props.port}/cards`)
            .then((res) => {
                // console.log(res.data);
                this.setState({
                    cards: res.data
                })
            })
            .catch(console.log);
    }

    getUsers = () => {
        axios.get(`http://${this.props.url}:${this.props.port}/users`)
            .then((res) => {
                // console.log(res.data);
                this.setState({
                    users: res.data
                })
            })
            .catch(console.log);
    }

    componentDidMount() {
        this.getServers();
        this.getCards();
        this.getUsers();

        setInterval(() => {
            this.getCards();
        }, 30000);
    }

    render() {
        console.log(this.state.editableProcess);
        let createTask = this.state.createTask;
        let editableProcess = this.state.editableProcess;

        return (
            <div className="workspace">
                <div
                    className="workspace__container"
                    style={{
                        backgroundColor: `${createTask || editableProcess ? '#F1F1F1' : 'inherit'}`,
                        boxShadow: `${createTask || editableProcess ? '0 0 1rem gray' : 'none'}`
                    }}
                >
                    <Table
                        mode={`${createTask || editableProcess ? 'select' : 'view'}`}
                        servers={this.state.servers}
                        cards={this.state.cards}
                        chosenCards={this.state.chosenCards}
                        returnChosen={chosen => this.setState({ chosenCards: chosen })}
                    />
                    {(!createTask && !editableProcess) && <button
                        className="workspace__button workspace__button_create-task"
                        // style={{ visibility: `${createTask ? 'hidden' : 'visible'}` }}
                        onClick={() => this.setState({ createTask: true })}
                    >Создать задачу</button>}
                    {(createTask || editableProcess) && <Form
                        url={this.props.url}
                        port={this.props.port}
                        cards={this.state.cards}
                        chosenCards={this.state.chosenCards}
                        users={this.state.users}
                        servers={this.state.servers}
                        editableProcess={this.state.editableProcess}
                        returnChosenCards={chosen => this.setState({ chosenCards: chosen })}
                        exitForm={status => {
                            if (status === 'edit') {
                                console.log(1, status === 'edit');
                                this.getCards();
                                return;
                            }
                            if (status === 'save') {
                                console.log(1, status === 'save');
                                this.getCards();
                            }
                            this.setState({
                                createTask: false,
                                editableProcess: null,
                                chosenCards: []
                            })
                        }}
                    />}
                </div>

                <Processes
                    url={this.props.url}
                    port={this.props.port}
                    createTask={this.state.createTask}
                    processDidDeleted={() => { this.getCards(); }}
                    processWillEdit={process => this.setState({
                        editableProcess: process
                    })}
                />

            </div>
        );
    }
}

export default Workspace;